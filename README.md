## Introduction
The Block Scheduler module is conditional plugin which adds block visibility
configuration such as publish and expiry dates to show & hide the block. This
is useful for showing or hiding blocks on a given date & time.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/block_scheduler

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/block_scheduler

## Install/Setup

Install module just like any other contrib module.

The only configuration that is needed is on the block configuration page.

See: https://www.drupal.org/documentation/install/modules-themes/modules-8
for further information.

## Configuration

The module has no menu or modifiable settings.

When enabled, the module will add new visibility settings on the block
configuration screen.

## Maintainers

nishkris
https://www.drupal.org/u/nishkris

George Anderson (geoanders)
https://www.drupal.org/u/geoanders
